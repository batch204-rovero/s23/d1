/*
	Objects
		An object is a data type that is used to represent a real world object
		It is a collection of related data and/or functionalities
		Information is stored in object represented in "key: value" pair
			key - property of the object
			value - actual data to be stored

		Two ways of creating object is JavaScript
		1. Object Literal Notation
			let/const objectName = {}
		2. Object Constructor Notation
			Object Instantiation (let object = new Object())

	Object Literal Notation
	Syntax:
		let/const = {
			keyA: valueA,
			keyB: valueB
		}
*/

let cellphone = {
	name: "Nokia 3210",
	manufactoringDate: 1999,
};

console.log("Result from creating objects using literal notation");
console.log(cellphone);

console.log(typeof cellphone);

let cellphone2 = {
	name: "Motorola",
	manufactoringDate: 2000,
}

console.log(cellphone2);

//Creating objects using constructor function
/*
	Syntax:
		function ObjectName(keyA, keyB) {
			this.keyA = keyA;
			this.keyB = keyB;
		}
*/

function Laptop(name, manufactureDate) {
	this.name = name;
	this.manufactureDate = manufactureDate;
}

let laptop = new Laptop('Lenovo', 2008);
console.log(laptop);

let myLaptop = new Laptop('MacBook', 2020);
console.log(myLaptop);

//Creating empty objects
let computer = {};
console.log(computer);

let myComputer = new Object();
console.log(myComputer);

//Accessing Object Properties

//Using the dot notation
console.log(myLaptop.name); //Macbook
console.log(myLaptop.manufactureDate); //2020

//Using the square bracket notation
console.log(myLaptop['name']);

//Accessing array objects
let array = [laptop, myLaptop];
console.log(array);

//arrayName[indexNumber].propertyName;
console.log(array[1].name); //Macbook

console.log(array[1]['name']); //Macbook

//Initializing/Adding/Deleting/Reassigning Object Properties
let car = {};
console.log(car);

//objectName.propertyName = value
car.name = 'Sarao';
console.log("Result from adding properties using dot notation:");
console.log(car);

car['manufacture date'] = 2019;
console.log(car);

//Deleting object properties
delete car['manufacture date'];
console.log(car);

car.manufactureDate = 2019;
console.log(car);

//Reassigning object properties
car.name = 'Ferrari SF-90';
console.log(car);

//Object Methods
/*
	A method is a function which is a property of an object
	They are also functions and one of the key differences they have is that methods are functions related to a specific object
	Methods are useful for creating object specific functions which are used to perform tasks on them
*/

let person = {
	name: "Aron",
	talkative: function() {
		console.log("Hello! My name is " + this.name);
	}
}

console.log(person);
person.talkative();

person.walk = function() {
	console.log(this.name + " walked 25 steps forward.");
}

person.walk();

//Methods are useful for creating reusable functions that performs tasks related to object

let friend = {
	firstName: "Hannah",
	lastName: "Lunzaga",
	address: {
		city: "Parañaque",
		country: "Philippines"
	},
	email: ['hannahlunzaga01@gmail.com'],

	introduce: function() {
		console.log("Hello! My name is " + this.firstName + " " + this.lastName);
	}
}

friend.introduce();

//Real World Application of Object
/*
	Scenario
		1. We would like to create a game that would have several pokemon interact with each other
		2. Every pokemon would have the same set of stats, properties, and functions
*/

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,

	tackle: function() {
		console.log("This pokemon tackled target pokemon");
		console.log("targetPokemon's health is now reduced to targetPokemonHealth");
	},

	faint: function() {
		console.log("Pokemon fainted");
	}
}

console.log(myPokemon);

//

function Pokemon(name, level, health) {
	//properties
	this.name = name;
	this.level = level;
	this.health = health;
	this.attack = level;

	//methods
	this.tackle = function(target) {
		console.log(this.name + " tackled " + target);
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack));
		target.health = target.health - this.attack;
		if (target.health <= 5) {
			target.faint()
		}
	},

	this.faint = function() {
		console.log(this.name + " fainted");
	}
}

let squirtle = new Pokemon("Squirtle", 99, 200);
let snorlax = new Pokemon("Snorlax", 75, 500);

console.log(squirtle);
console.log(snorlax);

snorlax.tackle(squirtle);

/*
	Mini-activity
	1. Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked
		(target.health - this.attack)

	2. If  health is less than or equal to 5, invoke faint function
*/

snorlax.tackle(squirtle);
snorlax.tackle(squirtle);